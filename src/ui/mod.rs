mod form;

use self::form::Model as Form;
use yew::prelude::*;

pub enum Message {}

pub struct Model {}

impl Component for Model {
    type Message = Message;
    type Properties = ();

    fn create(_properties: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Model {}
    }

    fn update(&mut self, _message: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <body>
                <header>
                    <Form/>
                </header>
            </body>
        }
    }
}
