use failure::{format_err, Error};
use serde_derive::Deserialize;
use yew::format::{Json, Nothing};
use yew::prelude::*;
use yew::services::fetch::{FetchService, FetchTask, Request, Response};
use yew::services::ConsoleService;

#[derive(Debug, Deserialize)]
pub struct File {
    value: String,
}

pub enum Message {
    EnterDown(KeyDownEvent),
    EnterUp(KeyUpEvent),
    FetchResponse(Result<File, Error>),
    InputChanged(String),
}

pub struct Model {
    fetch_callback: Callback<Response<Json<Result<File, Error>>>>,
    fetch_service: FetchService,
    fetch_task: Option<FetchTask>,
    input: Option<String>,
    is_fetching: bool,
    is_keying: bool,
    oninput_callback: Callback<InputData>,
    onkeydown_callback: Callback<KeyDownEvent>,
    onkeyup_callback: Callback<KeyUpEvent>,
}

impl Model {
    fn input_value(&self) -> String {
        let v;

        match &self.input {
            Some(input) => {
                v = input.clone();
            }
            None => {
                v = String::from("");
            }
        }

        v
    }

    fn load_input(&mut self) {
        ConsoleService::new().log(format!("load input: {}", self.input_value()).as_str());

        if !self.is_fetching {
            if let Some(input) = &self.input {
                self.is_fetching = true;

                let request = Request::get(input).body(Nothing).unwrap();

                let task = self
                    .fetch_service
                    .fetch(request, self.fetch_callback.clone());

                self.fetch_task = Some(task);
            }
        }
    }
}

impl Component for Model {
    type Message = Message;
    type Properties = ();

    fn create(_properties: Self::Properties, link: ComponentLink<Self>) -> Self {
        let fetch_callback = link.callback(|response: Response<Json<Result<File, Error>>>| {
            let (meta, Json(file)) = response.into_parts();
            if meta.status.is_success() {
                Message::FetchResponse(file)
            } else {
                Message::FetchResponse(Err(format_err!("{}: error getting file", meta.status)))
            }
        });

        let oninput_callback = link.callback(|event: InputData| Message::InputChanged(event.value));
        let onkeydown_callback = link.callback(Message::EnterDown);
        let onkeyup_callback = link.callback(Message::EnterUp);

        Model {
            fetch_callback,
            fetch_service: FetchService::new(),
            fetch_task: None,
            input: None,
            is_fetching: false,
            is_keying: false,
            oninput_callback,
            onkeydown_callback,
            onkeyup_callback,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        match message {
            Message::EnterDown(event) => {
                if (event.key() == "Enter") && !self.is_keying {
                    self.is_keying = true;
                }
            }
            Message::EnterUp(event) => {
                if (event.key() == "Enter") && self.is_keying {
                    self.load_input();

                    self.is_keying = false;
                }
            }
            Message::FetchResponse(result) => {
                debug_assert!(self.is_fetching);

                match result {
                    Ok(file) => {
                        ConsoleService::new().log(format!("fetched file: {:?}", file).as_str());
                    }
                    Err(error) => {
                        ConsoleService::new()
                            .log(format!("fetch failed with error: {:?}", error).as_str());
                    }
                }

                self.is_fetching = false;
            }
            Message::InputChanged(input) => {
                if input.chars().all(char::is_whitespace) {
                    self.input = None;
                } else {
                    self.input = Some(input);
                }
            }
        }

        true
    }

    fn view(&self) -> Html {
        html! {
            <input
                id="address",
                maxlength=5000,
                oninput=self.oninput_callback.clone(),
                onkeydown=self.onkeydown_callback.clone(),
                onkeyup=self.onkeyup_callback.clone(),
                placeholder="Enter a compatible SAFE XOR-URL",
                type="url",
                value=self.input_value()/>
        }
    }
}
