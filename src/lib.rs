mod ui;

use ui::Model as Ui;
use wasm_bindgen::prelude::*;
use yew::prelude::*;

#[wasm_bindgen]
pub fn run_app() -> Result<(), JsValue> {
    yew::initialize();

    App::<Ui>::new().mount_as_body();

    yew::run_loop();

    Ok(())
}
