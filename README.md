# Fetch SAFE

This project demonstrates a simple SAFE app, written in Rust using the Yew framework, deployed as WebAssembly and running in SAFE Browser.

When entering the XOR-URL of a compatible JSON file it will be fetched and its contents output to the console.


## System requirements

* Rust
* Cargo
* wasm-pack
* Rollup
* SAFE Vault (local or shared)
* SAFE CLI
* SAFE authd
* [SAFE Browser v0.16.0-alpha.2](https://github.com/maidsafe/safe_browser/releases/tag/v0.16.0-alpha.2)+


## 1) Build

* Run `scripts/build-develop`


## 2) Deploy

* Run `safe files put dist/ --recursive`


## 3) Run

1) Open the XOR-URL of the FilesContainer created when deploying with SAFE Browser
2) Enter the XOR-URL of fetchsafe.json to observe failure
3) Enter fetchsafe.json to observe success
