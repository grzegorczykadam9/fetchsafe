import init, { run_app } from './pkg/fetchsafe.js';
async function main() {
    await init('/pkg/fetchsafe_bg.wasm');
    run_app();
}
main()
